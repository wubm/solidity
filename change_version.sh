#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))
SOLIDITY_VERSION="$1"

if [ -z $SOLIDITY_VERSION ] ; then
    echo "Failed."
    echo "Usage: ./change_version.sh [version]"
    echo "       ex: ./change_version.sh v0.4.11"
    exit 1
fi

cd $DIR_PATH

sed -i "s/^SOLIDITY_VERSION=.*/SOLIDITY_VERSION=\"$SOLIDITY_VERSION\"/g" ./compile.sh
git add compile.sh
git commit -m "modify compile.sh to compile solidity $SOLIDITY_VERSION"
git push origin master
git tag -a $SOLIDITY_VERSION -m "Solidity $SOLIDITY_VERSION"
git push origin $SOLIDITY_VERSION
