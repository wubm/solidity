#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))
UPLOAD_URL="http://58.83.219.152:5001/api/v0/add"
SOLIDITY_VERSION="v0.4.11"

main () {
    BRANCH=$1
    VERSION=$2

    yum install -y epel-release
    yum install -y glibc-static libstdc++-static gcc-c++.x86_64 make m4 cmake3 cmake \
                   wget git python34-devel jsoncpp-devel protobuf-devel \
                   openssl-devel mariadb-devel mysql db4-devel libdb-cxx-devel
    
    compile_boost
    cd $DIR_PATH
    compile_solidity
    cd $DIR_PATH
    upload_solidity $BRANCH $VERSION

}

compile_boost() {
    wget http://sourceforge.net/projects/boost/files/boost/1.54.0/boost_1_54_0.tar.gz
    tar -zxf boost_1_54_0.tar.gz
    cd boost_1_54_0
    ./bootstrap.sh --prefix=/usr/local/boost_1_54_0
    ./b2 --with=all install link=static
}

compile_solidity() {
    git clone https://github.com/ethereum/solidity.git
    cd solidity
    git checkout $SOLIDITY_VERSION
    git submodule update --init --recursive

    sed -i "/cmake_minimum_required(VERSION 3.0.0)/ a \
                   set(Boost_INCLUDE_DIR \"/usr/local/boost_1_54_0/include\") \
                   \nset(BOOST_LIBRARYDIR \"/usr/local/boost_1_54_0/lib\") \
                   \nset(CMAKE_POSITION_INDEPENDENT_CODE ON) \
                   \nset(CMAKE_FIND_LIBRARY_SUFFIXES \"\.a\") \
                   \nset(BUILD_SHARED_LIBRARIES OFF) \
                   \nset(CMAKE_EXE_LINKER_FLAGS \"-static\")" \
                   ./CMakeLists.txt

    cmake3 -DCMAKE_BUILD_TYPE=Release -DTESTS=1 -DSTATIC_LINKING=1 .
    make
}

upload_solidity() {
    BRANCH=$1
    VERSION=$2

    if [ "$BRANCH" == "tag" ] ; then
        local solc_key=$(curl -i -F "file=@./solidity/solc/solc" "$UPLOAD_URL" \
                    | grep Hash | awk -F"\"" '{ print $8 }')
        local soltest_key=$(curl -i -F "file=@./solidity/test/soltest" "$UPLOAD_URL" \
                    | grep Hash | awk -F"\"" '{ print $8 }')
        local solfuzzer_key=$(curl -i -F "file=@./solidity/test/solfuzzer" "$UPLOAD_URL" \
                    | grep Hash | awk -F"\"" '{ print $8 }')
        local lllc_key=$(curl -i -F "file=@./solidity/lllc/lllc" "$UPLOAD_URL" \
                    | grep Hash | awk -F"\"" '{ print $8 }')
        if [ -n "$solc_key" ] ; then
            echo "Checkout Release"
            git checkout release
            echo "Log in README.md"
            sed -i "/^\* Solidity $VERSION.*/d" README.md
            echo "* Solidity $VERSION [solc $VERSION URL1](https://ipfs.io/ipfs/$solc_key) or \
                  [solc $VERSION URL2](https://gateway.ipfs.io/ipfs/$solc_key)" >> README.md
            echo "* Solidity $VERSION [soltest $VERSION URL1](https://ipfs.io/ipfs/$soltest_key) or \
                  [soltest $VERSION URL2](https://gateway.ipfs.io/ipfs/$soltest_key)" >> README.md
            echo "* Solidity $VERSION [solfuzzer $VERSION URL1](https://ipfs.io/ipfs/$solfuzzer_key) or \
                  [solfuzzer $VERSION URL2](https://gateway.ipfs.io/ipfs/$solfuzzer_key)" >> README.md
            echo "* Solidity $VERSION [lllc $VERSION URL1](https://ipfs.io/ipfs/$lllc_key) or \
                  [lllc $VERSION URL2](https://gateway.ipfs.io/ipfs/$lllc_key)" >> README.md
            echo "git config user.email"
            git config --global user.email "bingmu@gmail.com"
            echo "git config user.name"
            git config --global user.name "wubm"
            echo "git add README.md"
            git add README.md
            echo "git commit $VERSION"
            git commit -m "Solidity $VERSION"

            mkdir -p ~/.ssh
            cat $DIR_PATH/id_rsa > ~/.ssh/id_rsa
            chmod 600 ~/.ssh/id_rsa
            ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts

            echo "git push release"
            git push git@gitlab.com:wubm/solidity.git HEAD:release
        else
            echo "Upload faild!"
            exit 1
        fi

    fi
}

main "$@"
