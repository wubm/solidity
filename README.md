# Build Solidity Static
### How to trigger the build static binary
```bash change_version.sh [solidity_version]```

ex : ```bash change_version.sh v0.4.11```


### Download url at release branch 

https://gitlab.com/wubm/solidity/blob/release/README.md
